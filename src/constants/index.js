export const API_BASE_URL = 'http://aicam-env.3qq6b4wygm.us-east-1.elasticbeanstalk.com';
export const ACCESS_TOKEN = 'accessToken';
export const IMAGE_LIST = 'imageList';

export const OAUTH2_REDIRECT_URI = 'https://ai-cam-set.herokuapp.com/oauth2/redirect';

export const GOOGLE_AUTH_URL = API_BASE_URL + '/oauth2/authorize/google?redirect_uri=' + OAUTH2_REDIRECT_URI;

