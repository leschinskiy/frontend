import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import './AppHeader.css';
import Typography from "@material-ui/core/Typography";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";

class AppHeader extends Component {
    render() {
        return (
            <header className="app-header">

                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="h6" color="inherit" >
                            AICamSet
                        </Typography>
                        <nav className="app-nav">
                            { this.props.authenticated ? (
                                <ul>
                                     <li>
                                         <NavLink style={{
                                             color: "#9fa8da"
                                         }}  activeStyle={{
                                             fontWeight: "bold",
                                             color: "white"
                                         }} to="/profile">Profile</NavLink>
                                     </li>
                                    <li>
                                        <NavLink style={{
                                            color: "#9fa8da"
                                        }} activeStyle={{
                                            fontWeight: "bold",
                                            color: "white"
                                        }} to="/map">Map</NavLink>
                                    </li>
                                    <li>
                                        <NavLink onClick={this.props.onLogout} style={{
                                            color: "#9fa8da"
                                        }} activeStyle={{
                                            fontWeight: "bold",
                                            color: "white"
                                        }} to="/">Logout</NavLink>
                                    </li>
                                </ul>
                            ): (
                                <ul>
                                    <li>
                                        <NavLink activeStyle={{
                                            fontWeight: "bold",
                                            color: "white"
                                        }} style={{
                                            color: "#9fa8da"
                                        }} to="/login">Login</NavLink>
                                    </li>
                                    <li>
                                        <NavLink activeStyle={{
                                            fontWeight: "bold",
                                            color: "white"
                                        }} style={{
                                            color: "#9fa8da"
                                        }} to="/signup">Signup</NavLink>
                                    </li>
                                </ul>
                            )}
                        </nav>
                    </Toolbar>
                </AppBar>

            </header>

        )
    }
}

export default AppHeader;