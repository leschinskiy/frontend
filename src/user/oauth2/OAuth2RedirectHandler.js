import React, { Component } from 'react';
import {ACCESS_TOKEN, IMAGE_LIST} from '../../constants';
import { Redirect } from 'react-router-dom'
import {getImages} from "../../util/APIUtils";
import Alert from "react-s-alert";

class OAuth2RedirectHandler extends Component {
    getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');

        var results = regex.exec(this.props.location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    };
    uploadImages() {
        getImages()
            .then(response => {
                if (response.length !== 0) {
                    // let data = response.substring(1, response.length - 1);
                    sessionStorage.setItem(IMAGE_LIST, JSON.stringify(response));
                }
            }).catch(error => {
            Alert.error((error && error.message) || 'Oops! Something went wrong!');
        });
    }
    render() {        
        const token = this.getUrlParameter('token');
        const error = this.getUrlParameter('error');

        if(token) {
            localStorage.setItem(ACCESS_TOKEN, token);
            this.uploadImages();
            return <Redirect to={{
                pathname: "/profile",
                state: { from: this.props.location }
            }}/>; 
        } else {
            return <Redirect to={{
                pathname: "/login",
                state: { 
                    from: this.props.location,
                    error: error 
                }
            }}/>; 
        }
    }
}

export default OAuth2RedirectHandler;