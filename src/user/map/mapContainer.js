import React, {Component} from 'react';
import {GoogleApiWrapper, InfoWindow, Marker} from 'google-maps-react';

import CurrentLocation from './Map';

import {getImages} from "../../util/APIUtils";
import Alert from "react-s-alert";
import {IMAGE_LIST} from "../../constants";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

export class MapContainer extends Component {
    state = {
        showingInfoWindow: false,
        activeMarker: {},
        selectedPlace: {},
        image: {},
        images: JSON.parse(sessionStorage.getItem(IMAGE_LIST))
    };

    onMarkerClick = (props, marker, e) =>
        this.setState({
            image: props.image,
            selectedPlace: props,
            activeMarker: marker,
            showingInfoWindow: true
        });


    onClose = props => {
        if (this.state.showingInfoWindow) {
            this.setState({
                showingInfoWindow: false,
                activeMarker: null
            });
        }
    };
    createMarkers = () => {
        return this.state.images.map((item, index) => (
            <Marker onClick={this.onMarkerClick} image={item} key={item.id} position={{ lat: item.location.lat, lng: item.location.log}}>
            </Marker>
        ));

    };

    render() {
            if (this.state.images === null) {
                Alert.warning("You have not had any photo yet. Lets take it!");
                return (
                    <CurrentLocation centerAroundCurrentLocation google={this.props.google}>
                    </CurrentLocation>
                )
            } else {
                return (
                    <CurrentLocation centerAroundCurrentLocation google={this.props.google}>
                        {this.createMarkers()}
                        <InfoWindow marker={this.state.activeMarker} visible={this.state.showingInfoWindow} onClose={this.onClose} >
                            <Card style={{height: '450px', width: '300px'}}>
                                <CardActionArea>
                                    <CardMedia
                                        component="img"
                                        alt="Contemplative Reptile"
                                        image={'data:image/png;base64, '+this.state.image.base64Value}
                                    />
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            Aperture {this.state.image.aperture}
                                        </Typography>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            Shutter 1/{parseInt(this.state.image.shutterSpeed, 10)}
                                        </Typography>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            ISO {this.state.image.iso}
                                        </Typography>
                                    </CardContent>
                                </CardActionArea>
                            </Card>
                        </InfoWindow>
                    </CurrentLocation>

                );
            }
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyDk5y6A-JcPpdvuUej0YmwDUYeK40nsJsU'
})(MapContainer);
