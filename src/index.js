import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter as Router } from 'react-router-dom';
import App from "./app/App";
import MapContainer from "./user/map/mapContainer";


ReactDOM.render(
    <Router>
        <App>
        </App>
    </Router>,
    document.getElementById('root')
);

registerServiceWorker();
